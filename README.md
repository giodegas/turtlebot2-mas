The project work aims to implement a **multi-agent system** that is responsible of providing assistance to warehouse personnel.   
The fleet is composed by **Kobuki Turtlebot2** units, the scene represents a warehouse in which a conveyor belt carries packages into the room.  

Documentation can be found in */docs*

Demo: https://youtu.be/1dfWthhUovk

Authors:
- https://gitlab.com/valent0ne
- https://gitlab.com/Nimerya
- https://gitlab.com/AndreaPerel

